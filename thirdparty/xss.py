import os
from scrapy.cmdline import execute

class bclolors:
    RED="\033[01;31m"
    GREEN="\033[01;32m"    
    YELLOW="\033[01;33m"   
    BLUE="\033[01;34m"     
    BOLD="\033[01;01m"     
    RESET="\033[00m"

class xssers():
    def xssScrapy(self,app):
        os.chdir("thirdparty/xsscrapy/")
        execute(['scrapy', 'crawl', 'xsscrapy', '-a', 'url=%s' % app])
