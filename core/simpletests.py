import requests
import urllib3

class bclolors:
    RED="\033[01;31m"
    GREEN="\033[01;32m"    
    YELLOW="\033[01;33m"   
    BLUE="\033[01;34m"     
    BOLD="\033[01;01m"     
    RESET="\033[00m"

class simpleTests():
    def headerTests(self,app,payload):
        urllib3.disable_warnings()
        def requestResponse(rr):
            print bclolors.BLUE +"[+] " + bclolors.RESET + "Request"
            for x in rr.request.headers:
                print x + ': ' + rr.request.headers[x]
            print bclolors.BLUE +"[+] " + bclolors.RESET + "Response"
            print bclolors.BOLD + str(rr.status_code) + bclolors.RESET
            for x in rr.headers:
                print x + ': ' + rr.headers[x]

        get = requests.get(app,verify=False)
        print "\n"+bclolors.GREEN+"[*] "+bclolors.RESET+"Searching for cookie values in %s" % app
        for x in get.headers:
            if x == "Set-Cookie":
                if "HttpOnly" in get.headers[x]:
                    print bclolors.GREEN +"[+] " + bclolors.RESET + "HttpOnly Cookie looks fine in %s header" % x
                else:
                    print bclolors.BLUE +"[+] " + bclolors.RESET + "HttpOnly not set in %s header" % x
                    print get.headers[x]
                    
        print "\n"+bclolors.GREEN+"[*] "+bclolors.RESET+"Testing injection of %s in the Host header to %s" % (payload,app)
        get = requests.get(app, headers={'Host':payload},verify=False)
        if "2" in str(get.status_code):
            print bclolors.BLUE +"[+] " + bclolors.RESET + "Looks like the host header was accepted\n"
            requestResponse(get)

        print "\n"+bclolors.GREEN+"[*] "+bclolors.RESET+"Trying POST request to %s with %s as the referer header" % (app,payload)
        post = requests.post(app, headers={'Referer':payload},verify=False)
        if "2" in str(post.status_code):
            print bclolors.BLUE +"[+] " + bclolors.RESET + "Possible CSRF\n"
            requestResponse(post)
            print bclolors.YELLOW+"[!] "+bclolors.RESET+ "you may need authentication to the application to fully exploit"
        else:
            print bclolors.RED+"[!] "+bclolors.RESET+ "No CSRF this time"

        print "\n"+bclolors.GREEN+"[*] "+bclolors.RESET+"Trying GET request to %s with %s as the origin header" % (app,payload)
        get = requests.get(app, headers={'Origin':payload},verify=False)
        if "2" in str(get.status_code):
            print bclolors.BLUE +"[+] " + bclolors.RESET + "Looks like the origin header was accepted"
            requestResponse(get)
        else:
            print bclolors.RED+"[!] "+bclolors.RESET+ "%s was not accepted" % payload
