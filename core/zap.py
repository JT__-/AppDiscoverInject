import sys
import time
import urllib3
from pprint import pprint
from zapv2 import ZAPv2

class bclolors:
    RED="\033[01;31m"
    GREEN="\033[01;32m"    
    YELLOW="\033[01;33m"   
    BLUE="\033[01;34m"     
    BOLD="\033[01;01m"     
    RESET="\033[00m"

class zapscanner():
    def owaspZAP(self,app,apikey,zaplocation):
        urllib3.disable_warnings()
        try:
            zap = ZAPv2(apikey=apikey, proxies={'http': zaplocation, 'https': zaplocation})
            zap.urlopen(app)
        except:
            print '['+bclolors.RED+'!'+bclolors.RESET+'] could not connect to zap api'
            sys.exit(1)

        print '['+bclolors.GREEN+'+'+bclolors.RESET+'] Accessing target %s' % app
        time.sleep(2)
        
        print '['+bclolors.GREEN+'+'+bclolors.RESET+'] Spidering target %s' % app
        scanid = zap.spider.scan(app)
        time.sleep(2)
        
        while (int(zap.spider.status(scanid)) < 100):
            sys.stdout.write('['+bclolors.GREEN+'+'+bclolors.RESET+'] Spider progress: '+zap.spider.status(scanid)+'%\r')
            sys.stdout.flush()
            time.sleep(2)
        print '\n['+bclolors.GREEN+'+'+bclolors.RESET+'] Spider completed'
        time.sleep(5)

        print '['+bclolors.GREEN+'+'+bclolors.RESET+'] Scanning target %s' % app
        scanid = zap.ascan.scan(app)
        while (int(zap.ascan.status(scanid)) < 100):
            sys.stdout.write('['+bclolors.GREEN+'+'+bclolors.RESET+'] Scan progress: '+ zap.ascan.status(scanid)+'%\r')
            sys.stdout.flush()
            time.sleep(5)
        print '\n['+bclolors.GREEN+'+'+bclolors.RESET+'] Scan completed'
            
        f = open(zap.core.hosts[0]+'-xmlreport.xml','w')
        f2 = open(zap.core.hosts[0]+'-htmlreport.html','w')
        f.write(zap.core.xmlreport(apikey=apikey))
        f2.write(zap.core.htmlreport(apikey=apikey))

        f.close()
        f2.close()

        print '['+bclolors.GREEN+'+'+bclolors.RESET+'] wrote resutls to '+zap.core.hosts[0]+'-xmlreport.xml'
        print '['+bclolors.GREEN+'+'+bclolors.RESET+'] wrote results to '+zap.core.hosts[0]+'-htmlreport.html'

        zap.core.delete_all_alerts(apikey=apikey)
        zap.core.delete_site_node(app,apikey=apikey)
